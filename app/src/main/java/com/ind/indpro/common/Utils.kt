package com.ind.indpro.common

import android.content.Context
import android.net.ConnectivityManager
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.ind.indpro.R

/**
 * Here I'm displaying error image which has no data and error in image url
 */
@BindingAdapter("app:imageUrl")
fun ImageView.setImage(url: String?) {
    url?.let {
        Glide.with(this)
            .load(it)
            .error(R.drawable.ic_error_black_24dp)
            .apply(RequestOptions.circleCropTransform())
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(this)
    } ?: this.setImageResource(R.drawable.ic_error_black_24dp)
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}
