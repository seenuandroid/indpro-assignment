package com.ind.indpro.data.remote

import com.ind.indpro.data.models.DataModel

interface RemoteDataInterface {
    fun getData(res: (DataModel?) -> Unit)
}