package com.ind.indpro.data.remote

import android.content.Context
import android.util.Log
import com.ind.indpro.data.models.DataModel
import com.google.gson.GsonBuilder
import com.ind.indpro.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitDataSource(context: Context) : RemoteDataInterface {

    private val baseUrl = context.getString(R.string.baseUrl)
    private val retrofitService =
        getRetrofitService(baseUrl).create(APIInterface::class.java)

    override fun getData(res: (DataModel?) -> Unit) {
        val call = retrofitService.getData()
        call.enqueue(object : Callback<DataModel> {
            override fun onFailure(call: Call<DataModel>?, t: Throwable?) {
                Log.e(TAG, "Failed to retrieve Data")
                res(null)
            }

            override fun onResponse(call: Call<DataModel>?, response: Response<DataModel>?) {
                Log.d(TAG, "Successfully retrieved.")
                if (response?.isSuccessful == true) {
                    response.body()?.let { res(it) }
                } else {
                    res(null)
                }
            }
        })
    }

    companion object {
        val TAG = RetrofitDataSource::class.java.simpleName
        fun getRetrofitService(baseUrl: String): Retrofit {
            val client = OkHttpClient.Builder().addInterceptor(
                HttpLoggingInterceptor().setLevel(
                    HttpLoggingInterceptor.Level.BODY
                )
            )
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            return retrofit
        }
    }

}
