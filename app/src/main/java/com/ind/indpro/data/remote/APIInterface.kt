package com.ind.indpro.data.remote

import com.ind.indpro.data.models.DataModel
import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {

    @GET("facts.json")
    fun getData(): Call<DataModel>
}
