package com.ind.indpro.data.local

import com.ind.indpro.data.models.DataModel

interface LocalDataSource {
    fun insert(model: DataModel)
    fun update(model: DataModel)
    fun getLocalData(res: (List<DataModel>) -> Unit)
}