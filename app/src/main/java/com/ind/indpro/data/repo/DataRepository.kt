package com.ind.indpro.data.repo

import android.content.Context
import com.ind.indpro.data.local.LocalDataSource
import com.ind.indpro.data.local.MyDatabase
import com.ind.indpro.data.models.DataModel
import com.ind.indpro.data.remote.RemoteDataInterface
import com.ind.indpro.data.remote.RetrofitDataSource

open class DataRepository(context: Context) : RemoteDataInterface, LocalDataSource {
    private val remoteDataSource by lazy {
        RetrofitDataSource(context)
    }

    private val database by lazy {
        MyDatabase.getDatabase(context)
    }

    override fun getData(res: (DataModel?) -> Unit) {
        remoteDataSource.getData(res)
    }

    override fun insert(model: DataModel) {
        database?.dataDao()?.insert(model)
    }
    override fun update(model: DataModel) {
        database?.dataDao()?.update(model)
    }

    override fun getLocalData(res:(List<DataModel>)->Unit)  {
        database?.dataDao()?.getLocalData()?.let { res(it) }
    }


}