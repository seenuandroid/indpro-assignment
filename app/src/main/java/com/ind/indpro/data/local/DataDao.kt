package com.ind.indpro.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ind.indpro.data.models.DataModel

@Dao
interface DataDao {
    @Insert
    fun insert(model: DataModel)

    @Update
    fun update(model: DataModel)

    @Query("SELECT * FROM DataModel")
    fun getLocalData(): List<DataModel>
}