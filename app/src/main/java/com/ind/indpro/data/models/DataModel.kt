package com.ind.indpro.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.util.*


@Entity
data class RowsItem(
    @SerializedName("imageHref")
    val imageHref: String? = "",
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("title")
    val title: String? = null
)

@Entity
data class DataModel(
    @PrimaryKey(autoGenerate = true)
    @SerializedName("primaryId")
    @ColumnInfo(name = "primaryId")
    var id: Int,

    @SerializedName("title")
    @ColumnInfo(name = "title")
    val title: String? = null,

    @SerializedName("rows")
    @ColumnInfo(name = "rows")
    @TypeConverters(CustomTypeConverter::class)
    val rows: List<RowsItem>? = null
)


open class CustomTypeConverter {
    @androidx.room.TypeConverter
    fun storedStringToMyObjects(data: String?): List<RowsItem?>? {
        val gson = Gson()
        if (data == null) {
            return Collections.emptyList()
        }
        val listType = object : TypeToken<List<RowsItem?>?>() {}.type
        return gson.fromJson<List<RowsItem?>>(data, listType)
    }

    @androidx.room.TypeConverter
    fun myObjectsToStoredString(myObjects: List<RowsItem?>?): String? {
        val gson = Gson()
        return gson.toJson(myObjects)
    }
}