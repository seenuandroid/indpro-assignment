package com.ind.indpro.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ind.indpro.data.models.CustomTypeConverter
import com.ind.indpro.data.models.DataModel

@TypeConverters(CustomTypeConverter::class)
@Database(entities = [DataModel::class], version = 1)
abstract class MyDatabase : RoomDatabase() {
    abstract fun dataDao(): DataDao

    companion object {
        private const val DATABASENAME = "mydata.db"
        private var INSTANCE: MyDatabase? = null
        fun getDatabase(context: Context): MyDatabase? {
            if (INSTANCE == null) {
                synchronized(MyDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        MyDatabase::class.java, DATABASENAME
                    ).build()
                }
            }
            return INSTANCE
        }
    }
}