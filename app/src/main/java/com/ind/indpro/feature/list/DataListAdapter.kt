package com.ind.indpro.feature.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ind.indpro.data.models.RowsItem
import com.ind.indpro.databinding.ItemListBinding

class DataListAdapter(
    private val itemListener: ItemSelected
) : ListAdapter<RowsItem, DataListAdapter.Holder>(DIFF) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class Holder(private val itemListBinding: ItemListBinding) :
        RecyclerView.ViewHolder(itemListBinding.root) {
        fun bind(item: RowsItem) {
            itemListBinding.vm = item
            itemListBinding.root.setOnClickListener {
                itemListener.onSelect(getItem(adapterPosition))
            }
        }
    }


    companion object {
        private val DIFF = object : DiffUtil.ItemCallback<RowsItem>() {
            override fun areItemsTheSame(oldItem: RowsItem, newItem: RowsItem): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: RowsItem, newItem: RowsItem): Boolean {
                return oldItem.title == newItem.title
            }
        }
    }

}

interface ItemSelected {
    fun onSelect(item: RowsItem)
}