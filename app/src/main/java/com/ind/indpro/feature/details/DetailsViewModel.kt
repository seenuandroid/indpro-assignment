package com.ind.indpro.feature.details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ind.indpro.data.models.RowsItem

class DetailsViewModel(app: Application) : AndroidViewModel(app) {
    val obsDetails = MutableLiveData<RowsItem>()

    fun setDetails(details: RowsItem) {
        obsDetails.postValue(details)
    }
}