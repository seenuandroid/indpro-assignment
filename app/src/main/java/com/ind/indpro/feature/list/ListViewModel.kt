package com.ind.indpro.feature.list

import android.app.Application
import androidx.appcompat.view.menu.ListMenuItemView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ind.indpro.common.isNetworkAvailable
import com.ind.indpro.data.models.DataModel
import com.ind.indpro.data.repo.DataRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ListViewModel(app: Application) : AndroidViewModel(app) {

    private val job = Job()
    private val ioScope = CoroutineScope(Dispatchers.IO + job)
    private val repo by lazy {
        DataRepository(app)
    }
    val obsList = MutableLiveData<DataModel>()
    val obsTitle = MutableLiveData<String>()

    init {
        getRemoteData()
    }

    /**
     * Here we are getting data from API and saving into database
     * And notifying observer with data
     */
    fun getRemoteData() {
        suspend fun getLocalData(): DataModel? {
            return suspendCoroutine { cs ->
                repo.getLocalData {
                    cs.resume(it.lastOrNull())
                }
            }
        }

        //Getting API data when internet is connected otherwise,displaying local data
        if (isNetworkAvailable(getApplication())) {
            repo.getData { remoteData ->
                ioScope.launch {
                    val localData = getLocalData()
                    remoteData?.let {
                        it.id = ID
                        if (localData == null) {
                            repo.insert(it)
                        } else {
                            repo.update(it)
                        }
                    }
                    obsList.postValue(remoteData ?: localData)
                    obsTitle.postValue(remoteData?.title ?: localData?.title)

                }

            }

        } else {
            ioScope.launch {
                val localData = getLocalData()
                obsList.postValue(localData)
                obsTitle.postValue(localData?.title)
            }

        }
    }

    companion object {
        val TAG = ListMenuItemView::class.java.simpleName
        const val ID = 1
    }
}