package com.ind.indpro.feature.details


import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ind.indpro.data.models.RowsItem
import com.ind.indpro.databinding.FragmentDetailsBinding

/**
 * A simple [Fragment] subclass.
 */
class DetailsFragment() : Fragment() {
    private val mViewModel by lazy {
        ViewModelProvider(this).get(DetailsViewModel::class.java)
    }
    private var details: RowsItem? = null

    constructor(details: RowsItem) : this() {
        this.details = details
    }

    private var mBinding: FragmentDetailsBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentDetailsBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@DetailsFragment
        }
        return mBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Setting data to viewModel
        details?.let { mViewModel.setDetails(it) }

        //Setting up actionbar need like title, back button
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mViewModel.obsDetails.observe(viewLifecycleOwner, Observer {
            mBinding?.vm = it
            (requireActivity() as AppCompatActivity).supportActionBar?.title = it?.title
        })


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId === R.id.home) {
            requireActivity().onBackPressed()
            return true
        }
        return false
    }
}
