package com.ind.indpro.feature.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ind.indpro.MainActivity
import com.ind.indpro.data.models.RowsItem
import com.ind.indpro.databinding.FragmentListBinding
import com.ind.indpro.feature.details.DetailsFragment
import kotlinx.android.synthetic.main.fragment_list.*


class ListFragment : Fragment(), ItemSelected {
    private val mViewModel by lazy {
        ViewModelProvider(this).get(ListViewModel::class.java)
    }

    private val mAdapter by lazy {
        DataListAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@ListFragment
            vm = mViewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvList?.apply {
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
            adapter = mAdapter
        }
        swipeRefresh.isRefreshing = true
        mViewModel.obsList.observe(viewLifecycleOwner, Observer {
            it?.rows?.let { list ->
                mAdapter.submitList(list)
            }
            swipeRefresh.isRefreshing = false
        })
        mViewModel.obsTitle.observe(viewLifecycleOwner, Observer {
            setActionBarTitle()
        })
        swipeRefresh?.setOnRefreshListener {
            mViewModel.getRemoteData()
        }

    }

    private fun setActionBarTitle() {
        (requireActivity() as AppCompatActivity).supportActionBar?.title = mViewModel.obsTitle.value
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    override fun onSelect(item: RowsItem) {
        (requireActivity() as MainActivity).replaceFragment(DetailsFragment(item))
    }
}